package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class WallTest{
	Wall wall;
	@BeforeEach
	void setup() {
		this.wall = new Wall(0, 0, 100, 100, "grass");
	}
	
    @Test
	void TestgetHeight() {
	    assertEquals(wall.getHeight(), 100);
	}
	@Test
	void TestgetWidth() {
		assertEquals(wall.getWidth(), 100);
	}
	@Test
	void TestgetX() {
		assertEquals(wall.getX(), 0);
	}
	@Test
	void getY() {
	    assertEquals(wall.getY(), 0);
	}
	@Test
	void getSpeed() {
		assertEquals(wall.getSpeed(), 0);
	}
	@Test
	void getGravity() {
	    assertEquals(wall.getGravity(), 0);
	}
}
