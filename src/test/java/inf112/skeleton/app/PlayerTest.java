package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class PlayerTest{
	Player player;
	@BeforeEach
	void setup() {
		this.player = new Player();
	}
	
	@Test
	void MoveRightTest(){
		//Player player = new Player();
		player.canMoveDown = false;
		player.canMoveUp = false;
		player.canMoveLeft = false;
		player.canMoveRight = true;
		player.move_right = true;
		int x2 = player.getX();
		player.Move();
		assertEquals(player.getX(), player.getSpeed()+x2);
	}
	
	@Test
	void MoveLeftTest(){
		Player player = new Player();
		player.canMoveDown = false;
		player.canMoveUp = false;
		player.canMoveLeft = true;
		player.canMoveRight = false;
		player.move_left = true;
		int x2 = player.getX();
		player.Move();
		assertEquals(player.getX(), x2-player.getSpeed());
	}

	@Test
	void MoveUpTest(){
		Player player = new Player();
		player.canMoveDown = false;
		player.canMoveUp = true;
		player.canMoveLeft = false;
		player.canMoveRight = false;
		player.jump = true;
		int y2 = player.getY();
		player.Move();
		assertEquals(player.getY(), y2-6);
	}
	
	@Test
	void MoveDownTest(){
		Player player = new Player();
		player.canMoveDown = true;
		player.canMoveUp = false;
		player.canMoveLeft = false;
		player.canMoveRight = false;
		int y2 = player.getY();
		player.Move();
		assertEquals(player.getY(), y2+player.gravity);
	}
	
	@Test
	void DeadTest(){
		Player player = new Player();
		int heart = player.hearts;
		player.Dead();
		assertEquals(player.hearts, heart-1);
	}

    @Test
	void TestgetHeight() {
	    assertEquals(player.getHeight(), 80);
	}
    @Test
	void TestgetWidth() {
		assertEquals(player.getWidth(), 40);
	}
    @Test
	void TestgetX() {
		assertEquals(player.getX(), 45);
	}
    @Test
	void getY() {
	    assertEquals(player.getY(), 50);
	}
    @Test
	void getSpeed() {
		assertEquals(player.getSpeed(), 5);
	}
    @Test
	void getGravity() {
	    assertEquals(player.getGravity(), 5);
	}
}
