package inf112.skeleton.app;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Tree implements IGameObject{
    int x, y, w, h;
    String type;

    Image treeImg = new Image("file:src/PlayerImage/tree.png");
    Image pineImg = new Image("file:src/PlayerImage/pine.png");

    public Tree(int X, int Y, String type) {
        this.x = X;
        this.y = Y;
        this.w = 200;
        this.h = 230;
        this.type = type;
    }

    public int getHeight() {
        return this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    @Override
    public int getSpeed() {
        return 0;
    }

    @Override
    public int getGravity() {
        return 5;
    }

    public void drawTree(GraphicsContext gc, Player player) {
        int tree_w = this.w;
        int tree_h = this.h;

        double tree_xPos = (this.x - tree_w / 2.0) - player.getX() + player.screenX;
        double tree_yPos = (this.y - tree_h / 2.0);


        if (this.type == "tree") {
            gc.drawImage(treeImg, tree_xPos, tree_yPos, tree_w, tree_h);
        }
        else if (this.type == "pine") {
            gc.drawImage(pineImg, tree_xPos, tree_yPos, tree_w, tree_h);
        }
    }

}

