package inf112.skeleton.app;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Checkpoint implements IGameObject {
    private int x, y, w, h;
    Image checkpointImg = new Image("file:src/PlayerImage/checkpoint.png");
    boolean drawText;

    public Checkpoint(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 40;
        this.h = 60;
    }

    @Override
    public int getHeight() {
        return this.h;
    }

    @Override
    public int getWidth() {
        return this.w;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getSpeed() {
        return 0;
    }

    @Override
    public int getGravity() {
        return 0;
    }

    public void drawCheckpoint(GraphicsContext gc, Player player) {
        gc.drawImage(checkpointImg, (this.x - this.w/2.0) - player.getX() + player.screenX, this.y - this.h/2.0, this.w, this.h);
    }

    public void drawCheckpointText(GraphicsContext gc) {
        int width = 240;
        int height = 50;
        double x = (900/2.0) - width/2.0;
        double y = 100;

        gc.setFill(Color.rgb(0, 0, 0, 0.8));
        gc.fillRoundRect(x, y, width, height, 35, 35);

        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Comic sans MS", 20));
        gc.fillText("Checkpoint updated", x + 20, y + 30);
        gc.restore();
    }
}
