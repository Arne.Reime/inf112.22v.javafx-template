package inf112.skeleton.app;

import javafx.scene.input.KeyCode;

public class KeyHandlerStart {
    Game game;

    public KeyHandlerStart(Game game){
        this.game = game;
    }

    public void keyPressed(javafx.scene.input.KeyEvent evt) {

        KeyCode key = evt.getCode();

        if (key == KeyCode.ENTER && game.gameState == game.titleState) {
            game.setup();
        }

        if (key == KeyCode.H && game.gameState == game.titleState) {
            game.howToPlay();
        }

        if (key == KeyCode.ESCAPE && game.gameState == game.howToPlayState) {
            game.setupStart();
        }

        if (key == KeyCode.ENTER && game.gameState == game.deathState) {
            game.setupStart();
        }

    }
}
