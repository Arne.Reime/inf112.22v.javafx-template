package inf112.skeleton.app;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Waffle implements IGameObject {
    private int x, y, w, h;
    Image waffleImg =  new Image("file:src/PlayerImage/waffle.png");

    public Waffle(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 50;
        this.h = 50;
    }

    public int getHeight() {
        return this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    @Override
    public int getSpeed() {
        return 0;
    }

    @Override
    public int getGravity() {
        return 0;
    }

    public void drawWaffle(GraphicsContext gc, Player player) {
        int waffle_w = this.w;
        int waffle_h = this.h;

        double waffle_xPos = (this.x - waffle_w / 2.0) - player.getX() + player.screenX;
        double waffle_yPos = (this.y - waffle_h / 2.0);

        gc.drawImage(waffleImg, waffle_xPos, waffle_yPos, waffle_w, waffle_h);
    }

    public void drawWafflePoints(GraphicsContext gc, double x, double y, int w, int h) {
        gc.drawImage(waffleImg, x - w/2.0, y - h/2.0, w, h);
    }

    public void drawWaffleText(GraphicsContext gc, double w, double h) {
        int width = 400;
        int height = 200;
        double x = w/2 - width/2;
        double y = h/3.5 - height/2;

        gc.setFill(Color.rgb(0, 0, 0, 0.8));
        gc.fillRoundRect(x, y, width, height, 35, 35);

        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Comic sans MS", 20));
        gc.fillText("Hi! Waffle Thursday is closed and due\n to an error in the system, the very\n last waffle is made for " +
                "two people,\n you and Anya!!! Battle against Anya\n to secure the last waffle.", x + 20, y + 50);
        gc.restore();
    }

    public void waffleCount(GraphicsContext gc, Player player) {
        gc.setFill(Color.WHITE);
        gc.setFont(Font.font("Verdana", FontWeight.BOLD, 35));
        gc.fillText("x" + player.waffleScore, 65, 95);
        gc.restore();
    }

}
