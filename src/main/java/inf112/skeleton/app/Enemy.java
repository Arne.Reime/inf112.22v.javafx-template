package inf112.skeleton.app;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Enemy implements IGameObject{
    int x, y, w, h, drawW, distance;
    Image r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12;
    Image image;
    int walkSpeed = 2;
    int sum = 0;
    private int gravity = 5;
    public int runCounter = 0;
    boolean canMoveDown = true;
    int direction = 1;

    public Enemy(int X, int Y, int distance) {
        GetEnemyImage();
        this.x = X;
        this.y = Y;
        this.w = 40;
        this.h = 60;
        this.drawW = 70;
        this.distance = distance;
    }

    @Override
    public int getHeight() {
        return this.h;
    }

    @Override
    public int getWidth() {
        return this.w;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getSpeed() {
        return this.walkSpeed;
    }

    @Override
    public int getGravity() {
        return this.gravity;
    }

    public void walk() {
        if(direction == 1) {
            this.x += walkSpeed;
            sum += walkSpeed;
        }
        else {
            this.x -= walkSpeed;
            sum -= walkSpeed;
        }

        if (sum >= distance) direction = -1;

        else if (sum < 0) direction = 1;

        if (canMoveDown) {
            this.y += gravity;
        }
    }

    public void GetEnemyImage() {
        this.r1 = new Image("file:src/PlayerImage/enemyWalk1.png");
        this.r2 = new Image("file:src/PlayerImage/enemyWalk2.png");
        this.r3 = new Image("file:src/PlayerImage/enemyWalk3.png");
        this.r4 = new Image("file:src/PlayerImage/enemyWalk4.png");
        this.r5 = new Image("file:src/PlayerImage/enemyWalk5.png");
        this.r6 = new Image("file:src/PlayerImage/enemyWalk6.png");
        this.r7 = new Image("file:src/PlayerImage/enemyWalk7.png");
        this.r8 = new Image("file:src/PlayerImage/enemyWalk8.png");
        this.r9 = new Image("file:src/PlayerImage/enemyWalk9.png");
        this.r10 = new Image("file:src/PlayerImage/enemyWalk10.png");
        this.r11 = new Image("file:src/PlayerImage/enemyWalk11.png");
        this.r12 = new Image("file:src/PlayerImage/enemyWalk12.png");
    }

    public void drawEnemy(GraphicsContext gc, Player player) {
        if (runCounter > 55) {
            runCounter = 0;
        }

        //draw moving
        switch (runCounter) {
            case 0 -> this.image = r1;
            case 5 -> this.image = r2;
            case 10 -> this.image = r3;
            case 15 -> this.image = r4;
            case 20 -> this.image = r5;
            case 25 -> this.image = r6;
            case 30 -> this.image = r7;
            case 35 -> this.image = r8;
            case 40 -> this.image = r9;
            case 45 -> this.image = r10;
            case 50 -> this.image = r11;
            case 55 -> this.image = r12;
        }
        runCounter++;

        int enemy_w = this.drawW;
        int enemy_h = this.h;

        double enemy_xPos = (this.x - enemy_w*direction / 2.0) - 10*direction - player.getX() + player.screenX;
        double enemy_yPos = (this.y - enemy_h / 2.0);

        //hitbox:
        //gc.fillRect((this.x - w/2.0) - player.getX() + player.screenX, y - h/2.0, w, h);


        gc.drawImage(image, enemy_xPos, enemy_yPos, enemy_w*direction, enemy_h);
    }

}
