package inf112.skeleton.app;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Player implements IGameObject{
    private int x, y, w, h, drawW;
    public int speed = 5;
    public int gravity = 5;
    public int hearts = 3;
    public int waffleScore = 0;
    public int frames = 0;
    public int runCounter = 0;
    public int invincibleTimer = 100;

    int spawnX = 45;
    int spawnY = 50;

    boolean move_left = false;
    boolean move_right = false;
    boolean jump = false;
    boolean onGround = false;
    boolean invincible = false;

    boolean canMoveLeft, canMoveRight, canMoveDown, canMoveUp;

    public final double screenX;
    public final double screenY;

    int direction = 1;
    Image r1, r2, r3, r4, r5, r6, r7, r8;
    Image i1, i2, i3, i4, i5, i6, i7, i8;
    Image j1, j2;
    Image f1, f2;
    Image image;



    public Player(Game game) {
        PlayerValues();
        GetPlayerImage();

        screenX = game.height/2;
        screenY = game.width/1.3;
    }

    /**
     * Denne kontruktøren brukes bare for testing!
     */
    public Player(){
        PlayerValues();
        //GetPlayerImage();
        screenX = 0;
        screenY = 0;
    }

    public void PlayerValues() {
        this.x = 45;
        this.y = 50;
        this.w = 40;
        this.h = 80;
        this.drawW = 70;

    }

    public int getHeight() {
        return this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getSpeed() {
        return this.speed;
    }

    public int getGravity() {
        return this.gravity;
    }


    public void GetPlayerImage() {

        this.r1 = new Image("file:src/PlayerImage/Run1.png");
        this.r2 = new Image("file:src/PlayerImage/Run2.png");
        this.r3 = new Image("file:src/PlayerImage/Run3.png");
        this.r4 = new Image("file:src/PlayerImage/Run4.png");
        this.r5 = new Image("file:src/PlayerImage/Run5.png");
        this.r6 = new Image("file:src/PlayerImage/Run6.png");
        this.r7 = new Image("file:src/PlayerImage/Run7.png");
        this.r8 = new Image("file:src/PlayerImage/Run8.png");

        this.i1 = new Image("file:src/PlayerImage/Idle1.png");
        this.i2 = new Image("file:src/PlayerImage/Idle2.png");
        this.i3 = new Image("file:src/PlayerImage/Idle3.png");
        this.i4 = new Image("file:src/PlayerImage/Idle4.png");
        this.i5 = new Image("file:src/PlayerImage/Idle5.png");
        this.i6 = new Image("file:src/PlayerImage/Idle6.png");
        this.i7 = new Image("file:src/PlayerImage/Idle7.png");
        this.i8 = new Image("file:src/PlayerImage/Idle8.png");

        this.j1 = new Image("file:src/PlayerImage/Jump1.png");
        this.j2 = new Image("file:src/PlayerImage/Jump2.png");

        this.f1 = new Image("file:src/PlayerImage/Fall1.png");
        this.f2 = new Image("file:src/PlayerImage/Fall2.png");
    }

    public void Move() {
        if (move_left && canMoveLeft) {
            direction = -1;
            this.x -= this.speed;
        }

        if (move_right && canMoveRight){
            direction = 1;
            this.x += this.speed;
        }

        if (jump && canMoveUp) {
            onGround = false;
            if(frames < 25) {
                frames++;
                this.y -= 6;
            }
            else {
                frames = 0;
                jump = false;
                runCounter = 0;
            }
        }

        if(!jump && canMoveDown) {
            this.y += gravity;
            onGround = false;
        }
    }

    public void drawPlayer(GraphicsContext gc) {

        if (runCounter > 35) {
            runCounter = 0;
        }

        //draw moving left
        if (move_left && onGround) {
            switch (runCounter) {
                case 0 -> this.image = r1;
                case 5 -> this.image = r2;
                case 10 -> this.image = r3;
                case 15 -> this.image = r4;
                case 20 -> this.image = r5;
                case 25 -> this.image = r6;
                case 30 -> this.image = r7;
                case 35 -> this.image = r8;
            }
        }

        //draw moving right
        if (move_right && onGround) {
            switch (runCounter) {
                case 0 -> this.image = r1;
                case 5 -> this.image = r2;
                case 10 -> this.image = r3;
                case 15 -> this.image = r4;
                case 20 -> this.image = r5;
                case 25 -> this.image = r6;
                case 30 -> this.image = r7;
                case 35 -> this.image = r8;
            }
        }

        //draw not moving
        if (!move_left && !move_right && onGround) {
            switch (runCounter) {
                case 0 -> this.image = i1;
                case 5 -> this.image = i2;
                case 10 -> this.image = i3;
                case 15 -> this.image = i4;
                case 20 -> this.image = i5;
                case 25 -> this.image = i6;
                case 30 -> this.image = i7;
                case 35 -> this.image = i8;
            }
        }

        //draw jumping
        if (jump) {
            switch (runCounter) {
                case 0 -> this.image = j1;
                case 15 -> this.image = j2;
            }
        }

        //draw falling
        if (!jump && !onGround) {
            switch (runCounter) {
                case 0 -> this.image = f1;
                case 15 -> this.image = f2;
            }
        }

        runCounter++;

        //hitbox:
        //gc.fillRect(screenX - w/2.0, y - h/2.0, w, h);


        gc.drawImage(image, screenX-10*direction - drawW*direction/2.0, y - h/2.0, drawW * direction, h);
    }

    public void Dead() {
        this.x = this.spawnX;
        this.y = this.spawnY;
        this.hearts --;
    }

}
