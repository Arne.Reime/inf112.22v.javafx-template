package inf112.skeleton.app;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Wall implements IGameObject{
    private int x, y, w, h;
    String type;

    Image platformGrass = new Image("file:src/PlayerImage/grassplatform.png");
    Image box = new Image("file:src/PlayerImage/box.png");

    public Wall(int X, int Y, int W, int H, String type) {
        this.x = X;
        this.y = Y;
        this.w = W;
        this.h = H;
        this.type = type;
    }

    public int getHeight() {
        return this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    @Override
    public int getSpeed() {
        return 0;
    }

    @Override
    public int getGravity() {
        return 0;
    }

    public void drawWall(GraphicsContext gc, Player player) {
        int walls_w = this.w;
        int walls_h = this.h;

        double walls_xPos = (this.x - walls_w / 2.0) - player.getX() + player.screenX;
        double walls_yPos = (this.y - walls_h / 2.0);

        if (this.type == "grass") {
            gc.drawImage(platformGrass, walls_xPos, walls_yPos, walls_w, walls_h);
        }
        else if (this.type == "box") {
            gc.drawImage(box, walls_xPos, walls_yPos, walls_w, walls_h);
        }
    }

}
