package inf112.skeleton.app;

public interface IGameObject {

    int getHeight();

    int getWidth();

    int getX();

    int getY();

    int getSpeed();

    int getGravity();
}
