package inf112.skeleton.app;

import javafx.scene.input.KeyCode;

public class KeyHandler {
    Player player;

    public KeyHandler(Player player){
        this.player = player;
    }

    public void keyPressed(javafx.scene.input.KeyEvent evt) {

        KeyCode key = evt.getCode();

        if (key == KeyCode.LEFT) {
            player.move_left = true;
        }
        if (key == KeyCode.RIGHT) {
            player.move_right = true;
        }
        if ((key == KeyCode.UP || key == KeyCode.SPACE) && player.onGround) {
            player.jump = true;
            player.runCounter = 0;
        }

    }

    public void keyReleased(javafx.scene.input.KeyEvent evt) {
        KeyCode key = evt.getCode();

        if (key == KeyCode.LEFT ) {
            player.move_left = false;
            player.runCounter = 0;
        }
        if (key == KeyCode.RIGHT) {
            player.move_right = false;
            player.runCounter = 0;
        }
    }

}
