package inf112.skeleton.app;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Game extends Application {
    private AnimationTimer timer;
    private Canvas canvas;
    private long nanosPerStep = 1000_000_000L / 60L;
    private long timeBudget = nanosPerStep;
    private long lastUpdateTime = 0L;

    public double height = 600;
    public double width = 1000;

    double moveSky = 0.1;

    private List<Wall> ListWalls = new ArrayList<>();
    private List<Waffle> ListWaffle = new ArrayList<>();
    private List<Enemy> ListEnemy = new ArrayList<>();
    private List<Tree> ListTrees = new ArrayList<>();
    private List<Checkpoint> ListCheckpoint = new ArrayList<>();

    public Player player;
    public Waffle wafflePoint = new Waffle(1, 1);
    private Scene scene;

    Image hearts = new Image("file:src/PlayerImage/heart.png");
    Image waffleImg = new Image("file:src/PlayerImage/waffle.png");

    Image background1 = new Image("file:src/PlayerImage/freecutetileset/FreeCuteTileset/BG1.png");
    Image background2 = new Image("file:src/PlayerImage/freecutetileset/FreeCuteTileset/BG2.png");
    Image background3 = new Image("file:src/PlayerImage/freecutetileset/FreeCuteTileset/BG3.png");

    CollisionDetection cd = new CollisionDetection();

    public int gameState;
    public final int titleState = 0;
    public final int playState = 1;
    public final int deathState = 2;
    public final int howToPlayState = 3;


    public static void startIt(String[] args) {
        launch(args);
    }

    public Scene getScene() {
        return this.scene;
    }

    @Override
    public void start(Stage stage) {
        double width = this.width;
        double height = this.height;

        StackPane root = new StackPane();
        this.scene = new Scene(root, width, height);
        stage.setScene(scene);

        canvas = new Canvas(width, height);
        canvas.widthProperty().bind(scene.widthProperty());
        canvas.heightProperty().bind(scene.heightProperty());

        setupStart();


        timer = new AnimationTimer() {

            @Override
            public void handle(long now) {
                // System.out.println("Elapsed: " + (now -
                // lastUpdateTime)/(double)millisPerStep);
                if (lastUpdateTime > 0) {
                    timeBudget = Math.min(timeBudget + (now - lastUpdateTime), 10 * nanosPerStep);
                }
                lastUpdateTime = now;

                while (timeBudget >= nanosPerStep) {
                    // System.out.println("Budget: " + timeBudget);
                    timeBudget = timeBudget - nanosPerStep;
                    step();
                }
                draw();
            }

        };
        root.getChildren().add(canvas);

        timer.start();
        stage.show();
    }

    public void setupStart() {
        gameState = titleState;
        KeyHandlerStart keyS = new KeyHandlerStart(this);
        scene.setOnKeyPressed(keyS::keyPressed);
    }

    public void setupDeath() {
        gameState = deathState;
        KeyHandlerStart keyS = new KeyHandlerStart(this);
        scene.setOnKeyPressed(keyS::keyPressed);
    }

    public void howToPlay() {
        gameState = howToPlayState;
        KeyHandlerStart keyS = new KeyHandlerStart(this);
        scene.setOnKeyPressed(keyS::keyPressed);
    }

    public void setup() {
        gameState = playState;
        ListEnemy.clear();
        ListWalls.clear();
        ListWaffle.clear();
        ListTrees.clear();
        ListCheckpoint.clear();

        player = new Player(this);

        KeyHandler keyH = new KeyHandler(player);
        scene.setOnKeyPressed(keyH::keyPressed);
        scene.setOnKeyReleased(keyH::keyReleased);

        //SECTION 1

        ListTrees.add(new Tree(296, 315, "tree"));
        ListTrees.add(new Tree(900, 385, "tree"));

        ListWalls.add(new Wall(200, 480, 100, 100, "grass"));
        ListWalls.add(new Wall(296, 480, 100, 100, "grass"));

        int idx1 = 64;
        for (int i = 0; i < 12; i++) {
            ListWalls.add(new Wall(idx1, 550, 100, 100, "grass"));
            idx1 += 96;
        }

        ListEnemy.add(new Enemy(700, 250,  200));

        ListWaffle.add(new Waffle(300, 350));
        ListWaffle.add(new Waffle(1000, 400));

        //BREAK

        ListWalls.add(new Wall(1300, 390, 60, 60, "box"));
        ListWalls.add(new Wall(1360, 390, 60, 60, "box"));
        ListWalls.add(new Wall(1420, 390, 60, 60, "box"));

        //SECTION 2
        int idx2 = 1799;
        for (int i = 0; i < 5; i++) {
            ListWalls.add(new Wall(idx2, 440, 100, 100, "grass"));
            idx2 += 96;
        }

        int idx3 = 1696;
        for (int i = 0; i < 7; i++) {
            ListWalls.add(new Wall(idx3, 480, 100, 100, "grass"));
            idx3 += 96;
        }

        int idx4 = 1600;
        for (int i = 0; i < 8; i++) {
            ListWalls.add(new Wall(idx4, 550, 100, 100, "grass"));
            idx4 += 96;
        }

        ListTrees.add(new Tree(1880, 275, "tree"));
        ListWaffle.add(new Waffle(2300, 400));
        ListWalls.add(new Wall(2200, 360, 60, 60, "box"));

        //BREAK
        ListWalls.add(new Wall(2500, 360, 60, 60, "box"));
        ListWalls.add(new Wall(2800, 360, 60, 60, "box"));
        ListWalls.add(new Wall(3100, 360, 60, 60, "box"));

        //SECTION 3
        int idx5 = 3400;
        for (int i = 0; i < 10; i++) {
            ListWalls.add(new Wall(idx5, 550, 100, 100, "grass"));
            idx5 += 96;
        }
        ListCheckpoint.add(new Checkpoint(3600, 470));

        ListTrees.add(new Tree(3500, 385, "pine"));

        ListEnemy.add(new Enemy(3700, 250,  400));
        ListEnemy.add(new Enemy(3800, 250,  300));

        ListTrees.add(new Tree(4000, 385, "pine"));
        ListTrees.add(new Tree(4100, 385, "pine"));
        ListTrees.add(new Tree(4200, 385, "pine"));

        //BREAK
        ListWalls.add(new Wall(4400, 470, 60, 60, "box"));
        ListWalls.add(new Wall(4410, 325, 60, 60, "box"));
        ListWalls.add(new Wall(4720, 320, 60, 60, "box"));

        int idx6 = 5000;
        for (int i = 0; i < 10; i++) {
            ListWalls.add(new Wall(idx6, 320, 60, 60, "box"));
            idx6 += 60;
        }
        ListEnemy.add(new Enemy(5100, 200,  350));
        ListWaffle.add(new Waffle(5300, 120));

    }

    protected void step() {

        if (gameState == playState) {

            player.canMoveLeft = true;
            player.canMoveRight = true;
            player.canMoveDown = true;
            player.canMoveUp = true;

            for (Wall wall : ListWalls) {
                if (cd.checkCollision(player, wall, "right")) {
                    player.canMoveRight = false;
                }
                if (cd.checkCollision(player, wall, "left")) {
                    player.canMoveLeft = false;
                }
                if (cd.checkCollision(player, wall, "down")) {
                    player.canMoveDown = false;
                    player.onGround = true;
                }
                if (cd.checkCollision(player, wall, "up")) {
                    player.canMoveUp = false;
                    player.jump = false;
                    player.frames = 0;
                }
            }

            for (Waffle waffle : new ArrayList<Waffle>(ListWaffle)) {
                if (cd.checkCollision(player, waffle, "left") || cd.checkCollision(player, waffle, "right") ||
                        cd.checkCollision(player, waffle, "up") || cd.checkCollision(player, waffle, "down")) {
                    player.waffleScore++;
                    ListWaffle.remove(waffle);
                }
            }

            for (Enemy enemy : new ArrayList<Enemy>(ListEnemy)) {
                enemy.walk();
                if (cd.checkCollision(player, enemy, "down") && !player.jump) {
                    ListEnemy.remove(enemy);
                }
                if (cd.checkCollision(player, enemy, "left") && !player.invincible) {
                    player.hearts--;
                    player.invincible = true;
                }
                if (cd.checkCollision(player, enemy, "right") && !player.invincible) {
                    player.hearts--;
                    player.invincible = true;
                }
                for (Wall wall : ListWalls) {
                    if (cd.checkCollision(enemy, wall, "down")) {
                        enemy.canMoveDown = false;
                    }
                }
            }

            if (player.invincible) {
                if (player.invincibleTimer == 0) {
                    player.invincible = false;
                    player.invincibleTimer = 100;
                }
                player.invincibleTimer--;
            }

            player.Move();

            for (Checkpoint checkpoint : ListCheckpoint) {
                if (cd.checkCollision(player, checkpoint, "left") || cd.checkCollision(player, checkpoint, "right") ||
                        cd.checkCollision(player, checkpoint, "up") || cd.checkCollision(player, checkpoint, "down")) {
                    player.spawnX = checkpoint.getX();
                    player.spawnY = checkpoint.getY() - player.getHeight()/2;
                    checkpoint.drawText = true;
                }
                else {
                    checkpoint.drawText = false;
                }
            }

            if (player.getY() - player.getHeight() / 2.0 - 30 > this.height) {
                player.Dead();
            }

            if (player.hearts <= 0) {
                setupDeath();
            }
        }
    }

    protected void draw() {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        if (gameState == titleState) {
            Color c = Color.rgb(245, 187, 131);
            context.setFill(c);
            context.fillRect(0, 0, this.width, this.height);

            double x = this.width / 2;
            double y = this.height / 3;

            context.setFill(Color.WHITE);
            context.setFont(new Font("Comic sans MS", 60));
            context.fillText("Waffle Thursday", x - 240, y);
            context.setFont(new Font("Comic sans MS", 30));
            context.fillText("Press enter to play", x - 140, y + 80);
            context.fillText("Press h for how to play", x - 170, y + 130);
            context.restore();
        } else if (gameState == deathState) {
            Color c = Color.rgb(245, 187, 131);
            context.setFill(c);
            context.fillRect(0, 0, this.width, this.height);

            double x = this.width / 2;
            double y = this.height / 3;

            context.setFill(Color.WHITE);
            context.setFont(new Font("Comic sans MS", 60));
            context.fillText("Game Over", x - 160, y);
            context.setFont(new Font("Comic sans MS", 30));
            context.fillText("You died", x - 70, y + 70);
            context.fillText("Press enter to continue", x - 170, y + 120);
            context.restore();
        } else if (gameState == howToPlayState) {
            Color c = Color.rgb(245, 187, 131);
            context.setFill(c);
            context.fillRect(0, 0, this.width, this.height);

            double x = this.width / 2;
            double y = this.height / 3;

            context.setFill(Color.WHITE);
            context.setFont(new Font("Comic sans MS", 30));
            context.fillText("Reach the end of the map and \nacquire the final waffle to win.\n" +
                    "Jump on enemies to kill them.\n" +
                    "Collide with wooden posts to\n" +
                    "update your respawn location.", x - 210, y);
            context.fillText("Press esc to go back", x - 150, y + 300);
            context.restore();
        } else {
            context.drawImage(background1, 0, 0, this.width, this.height);

            int bg2Index = (int) (-moveSky / this.width) - 1;
            for (int i = 0; i < 3; i++)
                context.drawImage(background2, this.width * (bg2Index + i) - moveSky, 0, this.width, this.height);
            moveSky += 0.1;

            int bg3Index = (int) (player.getX() * 0.15 / this.width) - 1;
            for (int i = 0; i < 3; i++)
                context.drawImage(background3, this.width / 2 + this.width * (bg3Index + i) - player.getX() * 0.15 + player.screenX, 0, this.width, this.height);

            //TREE
            for (Tree tree : ListTrees) {
                tree.drawTree(context, player);
            }

            //WAFFLE DISPLAY
            context.drawImage(waffleImg, 10, 55, 50, 50);
            wafflePoint.waffleCount(context, player);
            if (player.getX() < 200) {
                wafflePoint.drawWaffleText(context, this.width, this.height);
            }

            //WALLS
            for (Wall wall : ListWalls) {
                wall.drawWall(context, player);
            }


            //ENEMY
            for (Enemy enemy : ListEnemy) {
                enemy.drawEnemy(context, player);
            }

            //WAFFLE
            for (Waffle waffle : ListWaffle) {
                waffle.drawWaffle(context, player);
            }

            //PLAYER
            player.drawPlayer(context);

            for (Checkpoint checkpoint : ListCheckpoint) {
                checkpoint.drawCheckpoint(context, player);
                if (checkpoint.drawText) {
                    checkpoint.drawCheckpointText(context);
                }
            }

            //HEARTS
            for (int i = 1; i <= player.hearts; i++) {
                int w = 45;
                int h = 45;
                double x = 50 * i - w / 2.0;
                double y = 50 - h / 2.0;

                context.drawImage(hearts, x - w / 2.0, y - h / 2.0, w, h);
            }
        }
    }
}


