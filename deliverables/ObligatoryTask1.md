Link til projectboard i [Asana](https://app.asana.com/0/1201785608693491/board)

Deloppgave 1)

Gruppenavn: Waffle-Run

Kompetanse:
Alle har lagd spill tidligere i INF101, Daniel, Erik og Martin har også lagd spill i INF100.
Daniel og Martin har også hatt INF214 som kan hjelpe når vi skal implementere flerspiller, ellers har alle hatt programmerings fag som INF100, INF101 og INF102

Roller:
Prosjekteier -Sigurd Blakkestad
Prosjekteieren er den som skal se på det store bildet, som skal se fremover. Sigurd ble prosjekt eier fordi han allerede har jobbet med asana før, og er flink til å ha oversikt oevr store prosjekt.

Prosjektleder -Årne Reime
Prosjektlederen er den som skal se på detaljene, på det som skjer nå. Årne fikk rollen som prosjektleder fordi han har mange gode innspill på hvordan detaljene i spillet skal være allerede, og er opptatt av at ting blir gjort skikkelig.

Kundekommunikasjon -Erik Jarle Gudesen Holmeide
Det er viktig å ha med en rolle som ansvarlig for kundekommunikasjon fordi det er viktig med en fast etablert kanal med kunden. Prosjektet skal utarbeides for å tilfredstille kundens behov, samtidig som praktiske begrensninger kan sette grenser, noe som gjør det helt essensielt for arbeidet å opprettholde en god aktiv dialog om prosessen med kunden. Det kan og oppstå hyppige ønskede endringer hos kunden. Erik ble valgt til ansvarlig for kundekommunikasjon fordi han er god på tydelig og konkret informasjonsformidling, samt god på kundeservice.

Strategi ansvarlig -Daniel Mjøs Røli
Det er viktig å ha en god plan for hvordan prosjektet skal skje, og en god strategi for å komme i mål med et prosjekt på best mulig måte. Daniel ble valgt som strategi ansvarlige fordi han går tredjeåret og har dermed mest erfaring med prosjekt arbeid og gruppe arbeid. Han har også god oversikt over de forskjellige prosjektmetodikkene.

Kvalitets sjekk/test ansvarlig -Martin Elias Toftevåg 
Testing er viktig for at koden skal funke slik vi så for oss at den skulle funke, og for å ungå unødvendige bugs.
Martin er har testet en del tidligere og er derfor kvalitets sjekk og test ansvarlig.
Viktig for å passe på at prosjektet møter krav som skal settes.
Lese over kode for å ungå så mange bugs og feil i kode som mulig.

Deloppgave 2)

KanBan bruker vi for oversikt over prosjekt progresjon og oppgave fordeling, vi bruker scrum gjennom hele prosjektet og fordeler arbeid, forholdsvis  tester vi mens vi koder, men etter vert skal vi legge til flere jUnit tester til koden.

Vi har valgt å møtes 2 ganger i uken, eller mer hvis det trengs. og et ekstra møte rett før innlevering for å finjustere det som trengs. Ene gangen vi møtes vil være gruppetimen på tirsdag, og andre gang vil planlegges på et tidspunkt alle kan ila uken. Vi har valgt å lage en ny gruppechat på discord hvor kommunikasjon mellom oss vil foregå, og har opprettet project board i Asana hvor vi har oversikt over arbeidsoppgaver og ting som er gjort.

Deloppgave 3)

Mål:

Vi bestemte oss tidlig for å lage et plattformspill basert på vaffeltorsdag der man skal navigere en spiller gjennom en bane for å rekke frem til premien i enden av løypen, vaffel. Vaffelen må rekkes innen en tidsfrist, og man skal bli utfordret av navigasjonen, fareområder og fiender på veien. 

Krav for første innlevering:

I muntlig refleksjon om utvikling av applikasjonen kom vi frem til følgende viktige aspekter vi ville implementere for denne første innleveringen: 
-	Ha et synlig spillbrett
-	Ha en synlig enkel overflate- og grafikk for bakgrunn
-	Ha en synlig spiller
-	Bevege spiller høyre og venstre
-	At spill-karakter kan hoppe

Disse punktene så vi samsvarer med å implementere de tre første kravene for MVP gitt i deloppgave 4. Vi utviklet deretter brukerhistorier tilknyttet MVP-kravene i deloppgave 4, samt har vi inkludert flere brukerhistorier vi føler samsvarer med viktige funksjoner for spillet vi ønsker å utvikle.

Brukerhistorier:
1.	Jeg som spiller trenger et spillbrett å se på for å ha noe visuelt å se på.
2.	Jeg som spiller trenger å ha en synlig spill-karakter på brettet for å vite hva jeg skal navigere på brettet.
3.	Jeg som spiller vil kunne flytte spill-karakteren til høyre og venstre for å utforske hele det tilgjengelige spillbrettet.
4.	Jeg som spiller ønsker at spill-karakter kan interagere med terreng for å få et tydelig bilde av hvilke plattformer som kan benyttes for navigeringen i terrenget
5.	Jeg som spiller har lyst til at spill-karakteren kan hoppe for å bytte plattformer eller hoppe på objekter.
6.	Jeg som spiller ønsker å tilegne poeng i spillet som gir et mål på hvor bra jeg gjør det.
7.	Jeg som spiller ønsker fiender på spillbrettet for å utfordres av mer mobile objekt og angrep.
8.	Jeg som spiller ønsker at spill-karakteren kan dø/gi ‘game over’ for å motiveres til å unngå skade mot spill-karakteren.
9.	Jeg som spiller ønsker et tydelig mål for spillbrettet for å motivere for progresjon i spillet. 
10.	Jeg som spiller ønsker en vaffel som målsymbol fordi det vil se kult ut og stemme med spillets tema/historie.
11.	Jeg som spiller ønsker et nytt spillbrett når det gamle er slutt for å kunne engasjeres av nye utfordringer og layouts.
12.	Jeg som spiller ønsker start-skjerm ved oppstart/’game over’ for å få lettvint tilgang til start-menyens valg.
13.	Jeg som spiller ønsker en synlig timer for å få en tydelig følelse for spillets tidspress.
14.	Jeg som spiller ønsker å ha en ‘boss-battle’ på slutten av spillet som kan gi en konkluderende avslutningsfølelse til spillet.
15.	Jeg som spiller ønsker støtte til ‘multiplayer’ for å oppleve gleden av å dele spillopplevelsen med andre folk.
16.	Jeg som spiller ønsker flere vanskelighets grader i spillet for å alltid kunne bli utfordret.
17.	Jeg som spiller ønsker at spillet blir vanskeligere jo lengre jeg kommer for å alltid kunne bli utfordret av spillet.

Brukerhistoriene vi prioriterte for denne innleveringen er historiene 1-5 over, som er tilknyttet de prioriterte aspektene vi listet over og samtidig svarer til MVP-krav 1-4 i deloppgave 4:

1.	Jeg som spiller trenger et spillbrett å se på for å ha noe visuelt å se på.
Akseptansekriterier: 
    - Designe spillbrett
    - Kode at spillbrettet vises i appen med canvas og set scene

2.	Jeg som spiller trenger å ha en synlig spill-karakter på brettet for å vite hva jeg skal navigere på brettet.
Akseptansekriterier:
    - Designe spillkarakter
    - Kode at designet vises på spillbrettet

3.	Jeg som spiller vil kunne flytte spill-karakteren til høyre og venstre for å utforske hele det tilgjengelige spillbrettet.
Akseptansekriterier:
    - Kode key-mapping til høyre og venstre piltast for spillkarakteren
    
4.	Jeg som spiller ønsker at spill-karakter kan interagere med terreng for å få et tydelig bilde av hvilke plattformer som kan benyttes for navigeringen i terrenget.
Akseptansekriterier:
    - Designe objekt spillkarakteren kan kollidere med som tydelig skiller seg fra bakgrunnen (som underlag og plattformer)
    - Kode wall grenser for alle kanter av disse objektene

5.	Jeg som spiller har lyst til at spill-karakteren kan hoppe for å bytte plattformer eller hoppe på objekter.
Akseptansekriterier:
    - Kode key-mapping til space piltast for å bevege spillkarakteren oppover
    - Kode substraksjon i negativ y retning for hvert frame opp til 25 frames




Deloppgave 4)
Først har vi en Game fil som fikser alt av setup, keyinputs, draw og step på spillbrettet. Først blir setup kalt som tegner inn spillbrettet med en karakter som spilleren kan bevege, vegger og golv som spilleren kan interagere med. Etterpå blir step kalt for hvert «frame». Denne sjekker posisjonen til spilleren i forhold til veggene på spillbrettet og stopper spilleren til å bevege seg videre viss spilleren sin x og y posisjon vil kollidere med veggene. Playeren har en bestemt gravitasjon som drar han nedover så lenge ikke føttene hans krasjer med golvet. For hvert frame blir også player.Move() kalt, denne sjekker for inputs fra brukeren og vil bevege seg etter hva key som blir presset (left, right, space). 

Deloppgave 5)
Til å begynne med gitt alt med planlegging av rollefordeliger basert på kompetanse veldig bra. Planleggingen gikk rask og enklet i samtale. Vi bestemte oss raskt for kanban struktur i bruk av Asana, og var alle tidlig enige i planlegging av hvordan vi skulle kommunisere og når vi skulle ha møter. Kommunikasjon via Discord har funket veldig bra, alle har vært delaktige her. Vi hadde en produktiv idemyldring om mål for appen der alle bidro med kreativitet og ideer, var raskt enige om hva vi skulle lage og hvilke funksjoner vi ville ha.
Vi hadde litt utfordringer med å sette oss inn i skjelett-prosjektene, vi kom ikke helt igang med god arbeidsfordelig og planlegging av arbeidsoppgaver. Det ble mer å bare få til noe som funket på denne fronten. Videre til neste oppgave vil vi da ha stor fokus på bedre arbeidsfordeling og planlegging i forhold til kode, for nå vet vi bedre hvordan det funker. 
Vi fikk ikke helt arbeidet i de rollene som vi fordelte, og mistet litt fokus på stragegien vi hadde valgt. Men vi jobbet som en god gruppe, samarbeidet og kommuniserte godt og kom godt i mål med det vi skulle gjøre. 
Vi fikk ikke gjort testingen skikkelig med JUnit siden vi brukte mye tid på å få oversikt. Men dette og er en proiritet for videre implementeringer neste levering. 
