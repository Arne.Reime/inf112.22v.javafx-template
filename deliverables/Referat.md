Referat 15.03:

-	Alle gruppemedlemmer var tilstede. Vi diskuterte hva som er plan for MVP videre, vi skal implementere enemies og designe ferdig spillbrett. Vi jobbet sammen for å utvikle JUnit tester for spillobjektene. Vi har og diskutert hvordan vi skal rydde opp i koden og fragmentere klassene bedre og planlagt komplett gjennomgang for å strukturere koden ved neste møte. Og vi har jobbet videre med multiplayer implementasjon og hvordan dette skal fungere grafisk på skjermen.


Referat 22.03.22:

- Alle var tilstede og vi gikk sammen gjennom koden for å planlegge sprinten før innlevering. Vi planla hva vi må implementere av tester for de forskjellige klassene, og vi har fordelt arbeidsoppgaver for å implementere tester, rydde i kode, skrive javadocs, implementere game over screen. Vi har planlagt nytt møte torsdag, og vi har gått gjennom vurdering for oblig 1 og diskutert hvordan vi skal forbedre oss.
